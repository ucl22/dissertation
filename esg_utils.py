import os
import numpy as np
import pandas as pd
from functools import cmp_to_key
from operator import add
import pydata_google_auth
import matplotlib.pyplot as plt
from sklearn.feature_selection import mutual_info_classif
import sklearn.metrics as skm
import statsmodels.formula.api as smf
import statsmodels.api as sm
from fancyimpute import SoftImpute

FI_CRIME_CLASS = "FI CRIME"
FI_PI_CLASS = "FI PI"
FI_D_O_CLASS = "FI D&O"
PI_US_CLASS = "PI US"
D_O_WW_CLASS = "D&O WW"
CYBER_CLASS = "CYBER PRIVACY & TECH"
ENERGY_MIDSTREAM_CLASS = "ENERGY MIDSTREAM"
ENERGY_OPERATIONAL_CLASS = "ENERGY OPERATIONAL"
ENERGY_LIABILITY_CLASS = "ENERGY LIABILITY"
ENERGY_CONSTRUCTION_CLASS = "ENERGY CONSTRUCTION"
ENERGY_GOM_WIND_CLASS = "ENERGY GOM WIND"

FI_CLASSES = [FI_CRIME_CLASS, FI_PI_CLASS, FI_D_O_CLASS]
ENERGY_CLASSES = [ENERGY_MIDSTREAM_CLASS, ENERGY_OPERATIONAL_CLASS, ENERGY_LIABILITY_CLASS, ENERGY_CONSTRUCTION_CLASS, ENERGY_GOM_WIND_CLASS]

E_REASON_1 = "E_REASON_CODE_1"
E_REASON_2 = "E_REASON_CODE_2"
E_REASON_3 = "E_REASON_CODE_3"
S_REASON_1 = "S_REASON_CODE_1"
S_REASON_2 = "S_REASON_CODE_2"
S_REASON_3 = "S_REASON_CODE_3"
G_REASON_1 = "G_REASON_CODE_1"
G_REASON_2 = "G_REASON_CODE_2"
G_REASON_3 = "G_REASON_CODE_3"

REASON_COLS = [E_REASON_1, E_REASON_2, E_REASON_3, S_REASON_1, S_REASON_2, S_REASON_3, G_REASON_1, G_REASON_2, G_REASON_3]

ESG_DATA_DEPTH = "ESG_DATA_DEPTH"
E_DATA_DEPTH = "ENVIRONMENTAL_DATA_DEPTH"
S_DATA_DEPTH = "SOCIAL_DATA_DEPTH"
G_DATA_DEPTH = "GOVERNANCE_DATA_DEPTH"

DEPTH_DISCOUNTS = {"A": 0, "B": 0, "C": 0.1, "D": 0.2, "E": 0.225, "F": 0.25, "G": 0.3, "H": 0.3, "I": 0.6}

DUNS_COL = "Duns"

ESG_COLS = [DUNS_COL, "COUNTRY", "COMPANY_NAME", "SECTOR_CATEGORY", "INDUSTRY", "ESG_RANKING", "E_RANKING", E_REASON_1,
            E_REASON_2, E_REASON_3, "S_RANKING", S_REASON_1, S_REASON_2, S_REASON_3,
            "G_RANKING", G_REASON_1, G_REASON_2, G_REASON_3, "ESG_RANKING_PEER_PERCENTILE",
            "ENVIRONMENTAL_RANKING_PEER_PERCE", "SOCIAL_RANKING_PEER_PERCENTILE", "GOVERNANCE_RANKING_PEER_PERCENTI",
            "ESG_RANKING_AVERAGE_PEER_SCORE", "ENVIRONMENTAL_RANKING_AVERAGE_PE", "SOCIAL_RANKING_AVERAGE_PEER_SCOR",
            "GOVERNANCE_RANKING_AVERAGE_PEER_", "ENVIRONMENTAL_RANKING_NATURAL_RE", "ENVIRONMENTAL_RANKING_EMISSIONS_",
            "ENVIRONMENTAL_RANKING_RISK_SCORE", "ENVIRONMENTAL_RANKING_OPPORTUNIT", "SOCIAL_RANKING_HUMAN_CAPITAL_SCO",
            "SOCIAL_RANKING_PRODUCT_SERVICE_S", "SOCIAL_RANKING_CUSTOMERS_SCORE", "SOCIAL_RANKING_COMMUNITY_SCORE",
            "SOCIAL_RANKING_SUPPLIER_SCORE", "SOCIAL_RANKING_CERTIFICATIONS_SC", "GOVERNANCE_RANKING_CORPORATE_GOV",
            "GOVERNANCE_RANKING_CORPORATE_BEH", "GOVERNANCE_RANKING_BUSINESS_RESI", "CLIMATE_RISK_SCORE", "ENERGY_MANAGEMENT_SCORE",
            "ENVIRONMENTAL_CERTIFICATIONS_SCO", "ENVIRONMENTAL_COMPLIANCE_SCORE", "ENVIRONMENTAL_OPPORTUNITIES_SCOR", "GHG_EMISSIONS_SCORE",
            "LAND_USE_AND_BIODIVERSITY_SCORE", "MATERIAL_SOURCING_AND_MANAGEMENT", "POLLUTION_PREVENTION_AND_MANAGEM",
            "WASTE_AND_HAZARDS_MANAGEMENT_SCO", "WATER_MANAGEMENT_SCORE", "COMMUNITY_ENGAGEMENT_SCORE", "CORPORATE_PHILANTHROPY_SCORE",
            "CYBER_RISK_SCORE", "DATA_PRIVACY_SCORE", "DIVERSITY_AND_INCLUSION_SCORE", "HEALTH_AND_SAFETY_SCORE", "HUMAN_RIGHT_ABUSES_SCORE",
            "LABOR_RELATIONS_SCORE", "PRODUCT_AND_SERVICES_SCORE", "PRODUCT_MANAGEMENT_AND_QUALITY_S", "SOCIAL_RELATED_CERTIFICATIONS_SC",
            "SUPPLIERS_ENGAGEMENT_SCORE", "TRAINING_AND_EDUCATION_SCORE", "BOARD_ACCOUNTABILITY_SCORE", "BUSINESS_ETHICS_SCORE",
            "BUSINESS_RESILIENCE_AND_SUSTAINA", "BUSINESS_TRANSPARENCY_SCORE", "CORPORATE_COMPLIANCE_BEHAVIORS_S", "GOVERNANCE_RELATED_CERTIFICATION",
            "SHAREHOLDER_RIGHTS_SCORE", ESG_DATA_DEPTH, E_DATA_DEPTH, S_DATA_DEPTH, G_DATA_DEPTH, "LOAD_YEAR", "LOAD_MONTH", "LOAD_DATE"]

ESG_COMPANY_COLS = ["COUNTRY", "SECTOR_CATEGORY", "INDUSTRY"]

ESG_RANKING = "ESG_RANKING"
E_RANKING = "E_RANKING"
S_RANKING = "S_RANKING"
G_RANKING = "G_RANKING"

E_S_G_RANKING_COLS = [E_RANKING, S_RANKING, G_RANKING]

ESG_THEME_COLS = ["ENVIRONMENTAL_RANKING_NATURAL_RE", "ENVIRONMENTAL_RANKING_EMISSIONS_", "ENVIRONMENTAL_RANKING_RISK_SCORE", "ENVIRONMENTAL_RANKING_OPPORTUNIT",
                  "SOCIAL_RANKING_HUMAN_CAPITAL_SCO", "SOCIAL_RANKING_PRODUCT_SERVICE_S", "SOCIAL_RANKING_CUSTOMERS_SCORE", "SOCIAL_RANKING_COMMUNITY_SCORE",
                  "SOCIAL_RANKING_SUPPLIER_SCORE", "SOCIAL_RANKING_CERTIFICATIONS_SC",
                  "GOVERNANCE_RANKING_CORPORATE_GOV", "GOVERNANCE_RANKING_CORPORATE_BEH", "GOVERNANCE_RANKING_BUSINESS_RESI"]

ESG_TOPIC_COLS = ["ENERGY_MANAGEMENT_SCORE", "WATER_MANAGEMENT_SCORE", "MATERIAL_SOURCING_AND_MANAGEMENT", "WASTE_AND_HAZARDS_MANAGEMENT_SCO",
                  "LAND_USE_AND_BIODIVERSITY_SCORE", "POLLUTION_PREVENTION_AND_MANAGEM", "GHG_EMISSIONS_SCORE", "CLIMATE_RISK_SCORE",
                  "ENVIRONMENTAL_COMPLIANCE_SCORE", "ENVIRONMENTAL_OPPORTUNITIES_SCOR", "ENVIRONMENTAL_CERTIFICATIONS_SCO",                  
                  "LABOR_RELATIONS_SCORE", "HEALTH_AND_SAFETY_SCORE", "TRAINING_AND_EDUCATION_SCORE", "DIVERSITY_AND_INCLUSION_SCORE",
                  "HUMAN_RIGHT_ABUSES_SCORE", "CYBER_RISK_SCORE", "PRODUCT_MANAGEMENT_AND_QUALITY_S", "PRODUCT_AND_SERVICES_SCORE",
                  "DATA_PRIVACY_SCORE", "CORPORATE_PHILANTHROPY_SCORE", "COMMUNITY_ENGAGEMENT_SCORE", "SUPPLIERS_ENGAGEMENT_SCORE", "SOCIAL_RELATED_CERTIFICATIONS_SC",
                  "BUSINESS_ETHICS_SCORE", "BOARD_ACCOUNTABILITY_SCORE", "SHAREHOLDER_RIGHTS_SCORE", "BUSINESS_TRANSPARENCY_SCORE",
                  "CORPORATE_COMPLIANCE_BEHAVIORS_S", "GOVERNANCE_RELATED_CERTIFICATION", "BUSINESS_RESILIENCE_AND_SUSTAINA"]

MODELLED_TOPICS = ["GHG_EMISSIONS_SCORE"]


COMPANY_COLS = ["Company_Name", "Employees_SS", "Employees_T", "Employees_BS",
                "Reporting_Currency", "Sales_T", "Sales_USD", "Ownership_Type", "Direct_Marketing_Status"]

COMPANY_SIZE_COLS = ["Employees_SS", "Employees_T", "Employees_BS", "Sales_T", "Sales_USD", "Ownership_Type"]

SQL_EXPIRY_DATE = "ExpiryDate"

ASIA = "Asia"
EUROPE = "Europe"
NORTH_AMERICA = "North_America"

COUNTRY_REGIONS = {
    "AT": EUROPE,
    "AU": ASIA,
    "BE": EUROPE,
    "CA": NORTH_AMERICA,
    "CH": EUROPE,
    "CZ": EUROPE,
    "DE": EUROPE,
    "DK": EUROPE,
    "FI": EUROPE,
    "FR": EUROPE,
    "GB": EUROPE,
    "HK": ASIA,
    "HR": EUROPE,
    "HU": EUROPE,
    "IE": EUROPE,
    "IN": ASIA,
    "KR": ASIA,
    "LU": EUROPE,
    "NL": EUROPE,
    "NO": EUROPE,
    "NZ": ASIA,
    "PL": EUROPE,
    "SA": ASIA,
    "SE": EUROPE,
    "SI": EUROPE,
    "SK": EUROPE,
    "TH": ASIA,
    "TW": ASIA,
    "US": NORTH_AMERICA
}

E_UNIVERSALLY_MATERIAL_REASONS = ["Environmental_compliance_-_Positive_Score", "Environmental_compliance_-_Elevated_Risk_score", "Environmental_compliance_-_Neutral_Score",
                                  "Environmental_opportunities_-_Positive_Score", "Environmental_opportunities_-_Elevated_Risk_score", "Environmental_opportunities_-_Neutral_Score",
                                  "Environmental_certifications_-_Positive_Score", "Environmental_certifications_-_Elevated_Risk_score", "Environmental_certifications_-_Neutral_Score"]
S_UNIVERSALLY_MATERIAL_REASONS = ["Diversity_and_inclusion_-_Positive_Score", "Diversity_and_inclusion_-_Elevated_Risk_score", "Diversity_and_inclusion_-_Neutral_Score",
                                  "Corporate_philanthropy_-_Positive_Score", "Corporate_philanthropy_-_Elevated_Risk_score", "Corporate_philanthropy_-_Neutral_Score",
                                  "Community_engagement_-_Positive_Score", "Community_engagement_-_Elevated_Risk_score", "Community_engagement_-_Neutral_Score",
                                  "Suppliers_engagement_-_Positive_Score", "Suppliers_engagement_-_Elevated_Risk_score", "Suppliers_engagement_-_Neutral_Score",
                                  "Social_related_certifications_-_Positive_Score", "Social_related_certifications_-_Elevated_Risk_score", "Social_related_certifications_-_Neutral_Score"]
G_UNIVERSALLY_MATERIAL_REASONS = ["Corporate_compliance_behaviors_-_Positive_Score", "Corporate_compliance_behaviors_-_Elevated_Risk_score", "Corporate_compliance_behaviors_-_Neutral_Score",
                                  "Governance_related_certifications_-_Positive_Score", "Governance_related_certifications_-_Elevated_Risk_score", "Governance_related_certifications_-_Neutral_Score",
                                  "Business_resilience_and_sustainability_-_Positive_Score", "Business_resilience_and_sustainability_-_Elevated_Risk_score", "Business_resilience_and_sustainability_-_Neutral_Score"]

# Filenames/paths
DATA_DIR = "Data"

ESG_DATA_FILE_NAME = "ESG_Data.csv"
BAD_ESG_FILE_NAME = "Bad_ESG.csv"
COMPANY_DATA_FILE_NAME = "Company_Data.csv"
POLICY_REFS_FILE_NAME = "Policy_Ref_Data.csv"
DUNS_FILE_NAME = "Duns.csv"

ESG_DATA_FILE_PATH = os.path.join(DATA_DIR, ESG_DATA_FILE_NAME)
BAD_ESG_FILE_PATH = os.path.join(DATA_DIR, BAD_ESG_FILE_NAME)
COMPANY_DATA_FILE_PATH = os.path.join(DATA_DIR, COMPANY_DATA_FILE_NAME)
POLICY_REFS_FILE_PATH = os.path.join(DATA_DIR, POLICY_REFS_FILE_NAME)
DUNS_FILE_PATH = os.path.join(DATA_DIR, DUNS_FILE_NAME)

E_S_G_RANKINGS = [E_RANKING, S_RANKING, G_RANKING]

E_S_G_RANKS = [1, 2, 3, 4, 5]

THEME_RANKS = [1, 2, 3]


gbq_credentials = pydata_google_auth.get_user_credentials(
    ['https://www.googleapis.com/auth/bigquery'],
    use_local_webserver=False
)
project_id = "ki-datalake-experiments-7b57"

def sql_query(query):
    df = pd.read_gbq(query, project_id=project_id, dialect='standard', credentials=gbq_credentials)
    return df


# Pull corresponding policies from DB
def get_policy_data(policy_refs, group_classes):
    policy_refs_list_string = ", ".join("'%s'" % str(ref) for ref in policy_refs)
    group_classes_list_string = ", ".join("'%s'" % str(key) for key in group_classes)
    
    policy_ref_criterium = "PolicyRef IN (" + policy_refs_list_string + ") AND " if len(policy_refs) > 0 else ""
    
    query = ("SELECT PolicyRef, GroupClass, InceptionDate, ExpiryDate, LifetimePolicyRef, PriorPolicyRef, StatsMajorClassDescription, Territory, Exposure_USD, ClaimCount " +
             "FROM `ki-datalake-prod-c82c`.`mymi_dump_live`.`Policy` " +
             "WHERE " + policy_ref_criterium +
             "GroupClass IN (" + group_classes_list_string + ") " +
             "AND InceptionDate < CAST('2020-01-01' AS DATE) AND InceptionDate > CAST('2011-12-31' AS DATE) " +
             # "AND PolicyYOA >= 2012 AND PolicyYOA <= 2020 " +
             "AND PlacingBasisGroup = 'OPEN MARKET' " +
             "AND Excess100Percent_USD = 0 " +
             "ORDER BY ExpiryDate")
    return sql_query(query)

def join_other_data(data, policy_ref_data):
    def assured_name_converter(val, default_val=""):
        try:
            return str(val).replace("\"", "")
        except Exception:
            return default_val
    
    def duns_converter(val, default_val=""):
        try:
            return str(val).replace("-", "")
        except Exception:
            return default_val
    
    def reason_converter(val, default_val=""):
        try:
            return str(val).replace(" ", "_")
        except Exception:
            return default_val
        
    def log10_converter(val, default_val=0):
        try:
            val = float(val)
            if val == 0:
                return default_val
            else:
                return np.log10(val)
        except Exception:
            return default_val
        
    esg_converters = {}
    for reason_col in REASON_COLS:
        esg_converters[reason_col] = reason_converter
    esg_converters[DUNS_COL] = duns_converter
    
    company_converters = {"Duns": duns_converter,
                          "Employees_BS": log10_converter,
                          "Sales_USD": log10_converter}
        
    esg_data = pd.read_csv(ESG_DATA_FILE_PATH, converters=esg_converters)
    esg_data.LOAD_DATE = pd.to_datetime(esg_data.LOAD_DATE, format="%Y%m%d").astype("M")    
    duns_data = pd.read_csv(DUNS_FILE_PATH, converters={"Duns": duns_converter, "AssuredName": assured_name_converter})
    company_data = pd.read_csv(COMPANY_DATA_FILE_PATH, converters=company_converters)
        
    esg_duns = esg_data.join(duns_data.set_index(DUNS_COL), on=DUNS_COL, how="inner")
    esg_duns_company = esg_duns.join(company_data.set_index(DUNS_COL), on=DUNS_COL, how="left")
    policy_an = data.join(policy_ref_data[["AssuredName", "PolicyReference"]].set_index("PolicyReference"), on="PolicyRef", lsuffix="_SQL", how="left")
    esg_policy = policy_an.set_index("AssuredName").join(esg_duns_company.set_index("AssuredName"), how="inner")
    
    esg_policy["Region"] = esg_policy["COUNTRY"].apply(lambda country: COUNTRY_REGIONS[country])
    
    return esg_policy[[DUNS_COL, "ClaimCount", SQL_EXPIRY_DATE, "Region", ESG_DATA_DEPTH] + ESG_TOPIC_COLS + ESG_COMPANY_COLS + COMPANY_COLS]    

def impute_esg_scores(data):
    def replace_strings(df):
        non_string_fields = list(df.columns.values)
        for column in list(df.columns.values):
            if pd.api.types.is_string_dtype(df.loc[:, column]):
                non_string_fields.remove(column)
                unique_vals = df.loc[:, column].unique()
                replace_dict = {}                
                for i in range(0, len(unique_vals)):
                    replace_dict[unique_vals[i]] = i
                                    
                df.loc[:, column] = df.loc[:, column].replace(replace_dict, inplace=False)
        return non_string_fields
    
    imputer = SoftImpute(min_value=1, max_value=5, verbose=False)
    impute_fields = ESG_TOPIC_COLS + ["Employees_SS", "Employees_T", "Employees_BS", "Sales_T", "Sales_USD", SQL_EXPIRY_DATE, ESG_DATA_DEPTH]
    df_unimputed = data.loc[:, impute_fields]
    non_string_impute_fields = replace_strings(df_unimputed)    
    unimputed_matrix = df_unimputed.to_numpy()    
    imputed_matrix = imputer.fit_transform(unimputed_matrix)    
    df_imputed = pd.DataFrame(imputed_matrix, columns=list(df_unimputed.columns), index=data.index)
    data.update(df_imputed.loc[:, non_string_impute_fields])
    
# set weights based on data depth
def set_weights(data, depth_discounts):
    data["weight"] = data[ESG_DATA_DEPTH].apply(lambda depth: 1 - depth_discounts[depth])
    
def print_nan_counts(data, max_missing):
    num_obs = len(data)
    bad_cols = []
    print("{:40s} \t Number of observations with no value".format("Column"))
    for column in list(data.columns.values):
        nan_count = data[column].isna().sum()
        print("{:40s} \t {:>15d}".format(column, nan_count))
        if (nan_count / num_obs > max_missing) or (column in MODELLED_TOPICS):
            bad_cols.append(column)
            
    return bad_cols

def print_stats_for_field(data, field, sum_over={}):
    def nan_compare(a, b):
        if str(a) == "nan":
            return 1
        if str(b) == "nan":
            return -1
        if a < b:
            return -1
        if a > b:
            return 1
        return 0
    
    print("{:25s} {:>25s} {:>25s} {:>25s}".format("Value", "Count", "Number with Claims", "Number without Claims"))
    unique_entries = sorted(data[field].unique(), key=cmp_to_key(nan_compare))
    print("%s counts:" % field)
    summed_vals = {}
    for key in sum_over:
        summed_vals[key] = (0, 0, 0)
        
    for entry in unique_entries:
        entry_data = data[data[field] == entry] if str(entry) != "nan" else data[data[field].isnull()]
        count = len(entry_data)
        claims_count = len(entry_data[entry_data.ClaimCount > 0])
        no_claims_count = len(entry_data[entry_data.ClaimCount == 0])
        print("{:25s} {:>25d} {:>25d} {:>25d}".format(str(entry), count, claims_count, no_claims_count))
        for key in sum_over:
            if entry in sum_over[key]:
                summed_vals[key] = list(map(add, (count, claims_count, no_claims_count), summed_vals[key]))
                break
        
    if len(summed_vals) > 0:
        print("\n")
        for key in summed_vals:
            count, claims_count, no_claims_count = summed_vals[key]
            print("{:25s} {:>25d} {:>25d} {:>25d}".format(str(key), count, claims_count, no_claims_count))
        
    print("\n\n")
        
def binarise_claims(data):
    data.ClaimCount = data.ClaimCount.apply(lambda x: min(x, 1))
    
def get_test_indices(train_set, test_set, features, cat_features):
    test_indices = np.full(len(test_set), True)
    for col in features:
        if col in cat_features:
            test_indices = np.logical_and(test_indices, test_set[col].isin(train_set[col].unique()))
            
    return test_indices

def formula_and_prediction_cols(prediction_cols, cat_prediction_cols, bad_cols=[]):
    formula_cols = []
    final_prediction_cols = []
    for col in prediction_cols:
        if col not in bad_cols:
            if col in cat_prediction_cols:
                formula_cols.append("C(" + col + ")")
            else:
                formula_cols.append(col)
            final_prediction_cols.append(col)

    formula = "ClaimCount ~ " + (" + ".join(formula_cols) if len(formula_cols) > 0 else "1")
    return formula, final_prediction_cols

def optimal_threshold_for_predictions(ground_truths, predictions):
        precision, recall, thresholds = skm.precision_recall_curve(ground_truths, predictions)
        f2_scores = (1 + 2**2) * (precision * recall) / ((2**2) * precision + recall)
        return thresholds[np.argmax(f2_scores)]

def fit_model(train_set, features, cat_features, bad_features=[]):
    formula, final_prediction_cols = formula_and_prediction_cols(features, cat_features, bad_features)
    train_data = train_set[["ClaimCount", "weight"] + final_prediction_cols].dropna()
    fitted_model = smf.glm(formula, family=sm.families.Binomial(), data=train_data, freq_weights=train_data.weight).fit()
    return fitted_model, final_prediction_cols, train_data
    
def fit_model_lasso(train_set, val_set, cont_prediction_cols, cat_prediction_cols=[], bad_cols=[]):  
    all_prediction_cols = cont_prediction_cols + cat_prediction_cols
    formula, final_prediction_cols = formula_and_prediction_cols(all_prediction_cols, cat_prediction_cols, bad_cols)
    
    max_auc = -1 * float("inf")
    best_alpha = -1
    for alpha in ALPHAS:
        train_data = train_set[["ClaimCount", "weight"] + final_prediction_cols].dropna()
        fitted_model = smf.glm(formula, family=sm.families.Binomial(), data=train_data, freq_weights=train_data.weight).fit_regularized(alpha=alpha, L1_wt=0.5, maxiter=100000, cnvrg_tol=0.0001)
        val_data = val_set[["ClaimCount"] + final_prediction_cols].dropna()
        val_predictions = fitted_model.predict(val_data)
        auc = skm.roc_auc_score(val_data["ClaimCount"], val_predictions)
        print("Alpha = {:f}; AUC = {:f}".format(alpha, auc))
        if auc > max_auc:
            max_auc = auc
            best_alpha = alpha
            
    print("Fitting with regularisation parameter alpha = {:f}".format(best_alpha))
        
    total_train_set = pd.concat([train_set, val_set])
    train_data = total_train_set[["ClaimCount", "weight"] + final_prediction_cols].dropna()
    fitted_model = smf.glm(formula, family=sm.families.Binomial(), data=train_data, freq_weights=train_data.weight).fit_regularized(alpha=best_alpha, L1_wt=0.5, maxiter=100000, cnvrg_tol=0.0001, refit=True)
    print(fitted_model.params)
    
    zero_cols = []
    for i in range(1, len(fitted_model.params)):
        if np.abs(fitted_model.params[i]) < 0.001:
            zero_cols.append(final_prediction_cols[i - 1])
            
    if len(zero_cols) == len(final_prediction_cols):
        formula = "ClaimCount ~ 1"
    else:
        formula, final_prediction_cols = formula_and_prediction_cols(all_prediction_cols, cat_prediction_cols, bad_cols + zero_cols)    
    return smf.glm(formula, family=sm.families.Binomial(), data=total_train_set[["ClaimCount"] + final_prediction_cols], freq_weights=total_train_set.weight).fit()


def fit_model_best_subset(train_set, val_set, cont_prediction_cols, cat_prediction_cols=[], bad_cols=[], force_cols=[]):
    all_prediction_cols = cont_prediction_cols + cat_prediction_cols
    formula, good_cols = formula_and_prediction_cols(all_prediction_cols, cat_prediction_cols, bad_cols)
        
    def auc_for_cols(cont_cols, cat_cols, force_cols):
        all_cols = cont_cols + cat_cols
        fitted_model, final_prediction_cols, train_data = fit_model(train_set, all_cols, cat_cols, bad_cols)
        contains_forced_col = len(force_cols) == 0
        if not contains_forced_col:
            for col in force_cols:
                if col in final_prediction_cols:
                    contains_forced_col = True
                    break
        if not contains_forced_col:
            return -1 * float("inf")
        val_data = val_set[["ClaimCount"] + final_prediction_cols].dropna()
        val_rows = get_test_indices(train_data, val_data, final_prediction_cols, cat_prediction_cols)        
        val_predictions = fitted_model.predict(val_data[val_rows])
        return skm.roc_auc_score(val_data.loc[val_rows, "ClaimCount"], val_predictions)
        
    def cont_cat_cols_for_indices(indices):
        cont_cols = []
        cat_cols = []
        for index in indices:
            col = good_cols[index]
            if col in cont_prediction_cols:
                cont_cols.append(col)
            else:
                cat_cols.append(col)
        return cont_cols, cat_cols
        
    def get_best_subset(included_indices):
        if len(included_indices) == 0:
            last_index = -1
        else:
            last_index = np.max(included_indices)
            
        cont_cols, cat_cols = cont_cat_cols_for_indices(included_indices)
        max_auc = auc_for_cols(cont_cols, cat_cols, force_cols)
            
        best_indices = included_indices
            
        if last_index < (len(good_cols) - 1):
            for index in range(last_index + 1, len(good_cols)):
                sub_max_auc, sub_best_indices = get_best_subset(included_indices + [index])
                if sub_max_auc > max_auc:
                    max_auc = sub_max_auc
                    best_indices = sub_best_indices
        
        return max_auc, best_indices
    
    max_auc, best_indices = get_best_subset([])
    cont_cols, cat_cols = cont_cat_cols_for_indices(best_indices)
    formula, prediction_cols = formula_and_prediction_cols(cont_cols + cat_cols, cat_cols, bad_cols)
    
    total_train_set = pd.concat([train_set, val_set])        
    
    fitted_model = smf.glm(formula, family=sm.families.Binomial(), data=total_train_set[["ClaimCount"] + prediction_cols], freq_weights=total_train_set.weight).fit()    
    
    return fitted_model, prediction_cols


def plot_model_curves(ground_truths, predictions, model_name):
    fpr, tpr, thresholds = skm.roc_curve(ground_truths, predictions)
    plt.plot(fpr, tpr, "ro")
    plt.title("ROC for {:s}".format(model_name))
    plt.plot([0, 1], [0, 1], linestyle="dashed")
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.show()
    print("Area under ROC curve: {:f}".format(skm.roc_auc_score(ground_truths, predictions)))
    
    print("\n")
    
    precision, recall, thresholds = skm.precision_recall_curve(ground_truths, predictions)
    plt.plot(recall, precision, "bo")
    plt.title("Precision-Recall for {:s}".format(model_name))
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.xlim(0, 1)
    plt.ylim(0, 1)
    plt.show()
    

########
# Graphing methods

def plot_dictionary(plot_dict, title=""):
    plt.bar(range(len(plot_dict)), list(plot_dict.values()), align='center')
    plt.xticks(range(len(plot_dict)), list(plot_dict.keys()))
    plt.title(title)
    plt.show()
    
def plot_ranks_for_field(data, field, ranks):
    esg_rank_counts = {}
    for rank in ranks:
        esg_rank_counts[rank] = len(data[data[field] == rank])
        
    plot_dictionary(esg_rank_counts, field)
    
def plot_topic_violins(data, title="", print_key=True):
    fig, ax = plt.subplots(figsize=(30, 10), dpi=80)
    violins = []
    all_cols_violin = []
    labels = []
    counts = []
    for col in ESG_TOPIC_COLS:
        violin = data[col].dropna().to_numpy()
        counts.append(len(violin))
        if len(violin) == 0:
            violin = [-10]            
        else:
            all_cols_violin.extend(violin)            
        violins.append(violin)        
        labels.append(col)        
        
    violins.append(all_cols_violin)
    labels.append("All Columns")
    counts.append(len(all_cols_violin))
    x_locs = range(1, len(violins) + 1)
    ax.violinplot(violins)
    plt.xticks(x_locs)
    plt.title(title)
    plt.xlabel("Topic")
    plt.ylabel("Score")
    plt.show()

    if print_key:
        print("Key:")
        for i in range(0, len(x_locs)):
            x_loc = x_locs[i]
            print("{:d} \t {:30s} \t {:>5d}".format(x_loc, labels[i], counts[i]))
    
        
def print_topic_correlations(data, obs_threshold, val_threshold=0.2):    
    print("Correlation coefficients amongst predictors:")
    print("{:35s} \t {:35s} \t\t {:>20s} \t\t {:>20s}".format("Predictor 1", "Predictor 2", "Correlation Coefficient", "Num Obs"))
    for i in range(0, len(ESG_TOPIC_COLS) - 1):
        for j in range(i + 1, len(ESG_TOPIC_COLS)):
            col_1 = ESG_TOPIC_COLS[i]
            col_2 = ESG_TOPIC_COLS[j]
            cols_data = data.loc[:, [col_1, col_2]].dropna()
            if len(cols_data[col_1].unique()) > 1 and len(cols_data[col_2].unique()) > 1:
                corr = np.corrcoef([cols_data[col_1], cols_data[col_2]])[0, 1]
                if (len(cols_data) > obs_threshold) and (np.abs(corr) > val_threshold):
                    print("{:35s} \t {:35s} \t\t {:>15s}{:>.5f} \t\t {:>15s}{:>d}".format(col_1, col_2, "", corr, "", len(cols_data)))
